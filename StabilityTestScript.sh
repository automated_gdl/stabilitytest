#!/bin/bash
#This test script aims to stress middleware and launcher.

Bla='\e[0;30m'; On_Red='\e[41m'; On_Gre='\e[42m'; BYel='\e[1;33m'; Cya='\e[0;36m'; BIBlu='\e[1;94m'; Gre='\e[0;32m'; Red='\e[0;31m'; Pur='\e[0;35m'; # Text colors
RCol='\e[0m' # Text Reset

sleep 20
settings put secure sleep_timeout -1 #Set sleep tomeput to never

#Completes profiles screen by selecting main profile and input default PIN (0000).
run_profiles() {
  Profiles=$(dumpsys window windows | grep -E mCurrentFocus)
  if [[ $Profiles == *"ProfileManagementActivity"* ]]; then
    sleep 5
    input keyevent KEYCODE_BACK      #Closes PIN menu if opened
    input keyevent KEYCODE_DPAD_DOWN #LOCATES SELECTION TO MAIN PROFILE
    input keyevent KEYCODE_DPAD_DOWN 
    input keyevent KEYCODE_DPAD_LEFT
    input keyevent KEYCODE_DPAD_LEFT
    input keyevent KEYCODE_DPAD_LEFT
    input keyevent KEYCODE_DPAD_LEFT
    input keyevent KEYCODE_DPAD_CENTER
    sleep 1
    input text "0000"
  fi
  return
}

sleep 5
run_profiles

cd sdcard/Automation
echo $(getprop ro.boot.bootreason) >> Boot_Reasons

#Reads device under test information
Model=$(getprop ro.product.vendor.device)
Snum=$(getprop ro.serialno)
MacA=$(getprop ro.boot.mac)
Aver=$(getprop ro.build.version.release)
Build=$(getprop ro.product.version.base)
Launcher=$(dumpsys package com.dotscreen.megacable.livetv | grep versionName | cut -d "=" -f2)
IPv4=$(ip route | cut -d " " -f9)
#Apro=$(logcat -d | grep modelExternalId | cut -d " " -f 11)
timestamp1=$(date "+%Y-%m-%d_%H-%M-%S") #Update timestamp for logs name
startDate=$(date "+%Y-%m-%d")
crashLog1=0; LiveTVcrashCount=0; DtvCrashCount=0; SysCrashCount=0; MulticastCrashCount=0; PortalCrashCount=0; TotalCrashCount=$(sed -n 3p InstabilityEvents | cut -d "," -f7); varCount=0; testCount=$(sed -n 3p InstabilityEvents | cut -d "," -f1); CountTIF=0
TestN=0; vCount=0; aCount=0; ChannelN=0; LoadCPU=0; FreeRAM=0; AvgCPU=0; SumCPU=0; AvgRAM=0; SumRAM=0; MBF=0; AvgMBF=0; SumMBF=0; TIF=0; RebootCount=0
TestCount=$(printf "%03d" "$TestCount")
TotalCrashCount=$(printf "%03d" "$TotalCrashCount")
sleep .5

#Prints device under test information.
input keyevent keyevent KEYCODE_HOME
sleep 10
type=$(logcat -d | grep "HomeHeroZoneView: tifChannel")
input keyevent KEYCODE_DPAD_UP
input keyevent KEYCODE_DPAD_UP
input keyevent KEYCODE_DPAD_UP
input keyevent KEYCODE_DPAD_UP
type=$(logcat -d | grep "HomeHeroZoneView: tifChannel")
if [[ $type == *"TYPE_DVB_C"* ]]; then
  Apro="DVB"
else
  Apro="IPTV"
fi

echo "$Model"-"$Apro" > DeviceInfo
echo "Serial Number","$Snum" >> DeviceInfo
echo "MAC Address","$MacA" >> DeviceInfo
echo "Build","${Build:0:6}" >> DeviceInfo
echo "Launcher","${Launcher:0:6}" >> DeviceInfo

run_profiles

#Here, script will start with the automatic set up, go to home and tune last channel.
am start -n com.dotscreen.megacable.livetv/com.dotscreen.tv.MainActivity
sleep 5
Channel=$(logcat -d | grep displayNumber | grep onZap | cut -d "," -f5 | cut -b16-19) #Update current channel.
ChannelN=$(printf "%04d" "${Channel:(-4)}")

echo $BYel"------------------------------------------Test begins here!------------------------------------------" 
#Read PID from live tv Launcher
StartLivetvPID=$(pidof com.dotscreen.megacable.livetv)
CurrtLivetvPID=$(pidof com.dotscreen.megacable.livetv)
LiveTVcrashCount=$(sed -n 3p InstabilityEvents | cut -d "," -f2)

#Read PID from portal Launcher
StartPortalPID=$(pidof com.dotscreen.megacable.portal)
CurrtPortalPID=$(pidof com.dotscreen.megacable.portal)
PortalCrashCount=$(sed -n 3p InstabilityEvents | cut -d "," -f3)

#Read PID from net.oregan.tvinput.dtv middleware
StartDtvPID=$(pidof net.oregan.tvinput.dtv)
CurrtDtvPID=$(pidof net.oregan.tvinput.dtv)
DtvCrashCount=$(sed -n 3p InstabilityEvents | cut -d "," -f4)

#Read PID from net.oregan.tvinput.multicast middleware
StartMulticastPID=$(pidof net.oregan.tvinput.multicast)
CurrtMulticastPID=$(pidof net.oregan.tvinput.multicast)
MulticastCrashCount=$(sed -n 3p InstabilityEvents | cut -d "," -f5)

#Read PID from System server
StartSysServPID=$(pidof system_server)
CurrtSysServPID=$(pidof system_server)
SysCrashCount=$(sed -n 3p InstabilityEvents | cut -d "," -f6)

TotalCrashCount=$(sed -n 3p InstabilityEvents | cut -d "," -f7)

#Checks whether middleware crashed.

sys_crash() {
  CurrtSysServPID=$(pidof system_server)
  if [ $StartSysServPID -eq "$CurrtSysServPID" ]; then #
    return
  else
    crashLog1=$(logcat -d AndroidRuntime:E DEBUG:F *:S > ./Crashes/$timestamp1-SystemServer-Crash.txt)
    SysCrashCount=$(($SysCrashCount + 1)) #Increase counter.
    TotalCrashCount=$(($TotalCrashCount + 1)) #Increase counter.
    curr_time="$(date -u +%s)"
    MBF="$(($curr_time-$start_time))"
    SumMBF=$(($SumMBF+$MBF))
    echo $SumMBF > AvgMBF
    CurrMBF="$(printf '%02d:%02d:%02d\n' $(($MBF/3600)) $(($MBF%3600/60)) $(($MBF%60)))"
    echo "---------------------------- ${Gre}Channel Zapping and banner navigation test${RCol} ---------------------------" ;
    echo "                        ""$Bla""$On_Red""¡ANDROID system_server CRASHED!""$RCol""                                 ";
    echo "${RCol}${TimeStamp:0:19} -> ${Cya}TEST#$RCol $TestN ${Cya}Ch${RCol} $ChannelN ${Cya}Crashes${RCol} $TotalCrashCount ${Cya}Reboots${RCol} $RebootCount ${Cya}CPU Load${RCol} $LoadCPU ${Cya}Free RAM${RCol} ${FreeRAM}MB${Cya} MBF${RCol} $CurrMBF${RCol}"
    echo "${TimeStamp:0:19},$TestN,$ChannelN,$TotalCrashCount,$RebootCount,$LoadCPU,$FreeRAM,$CurrMBF" >> Testresults;
    start_time="$(date -u +%s)"
    echo $start_time > MBF
    sleep 5
    logcat -c
    sleep 20
    StartSysServPID=$(pidof system_server)
    StartDtvPID=$(pidof net.oregan.tvinput.dtv)
    StartMulticastPID=$(pidof net.oregan.tvinput.multicast)
    StartLivetvPID=$(pidof com.dotscreen.megacable.livetv)
    StartPortalPID=$(pidof com.dotscreen.megacable.portal)
    #RebootCount=$(settings get global boot_count)
    #RebootCount=$(($RebootCount - 1))
    #settings put global boot_count $RebootCount
    main
  fi
}

dtv_crash() {
  CurrtDtvPID=$(pidof net.oregan.tvinput.dtv)
  if [ $StartDtvPID -eq "$CurrtDtvPID" ]; then #
    return
  else
    crashLog1=$(logcat -d AndroidRuntime:E DEBUG:F *:S > ./Crashes/$timestamp1-DTV-Crash.txt)
    DtvCrashCount=$(($DtvCrashCount + 1)) #Increase counter.
    TotalCrashCount=$(($TotalCrashCount + 1)) #Increase counter.
    curr_time="$(date -u +%s)"
    MBF="$(($curr_time-$start_time))"
    SumMBF=$(($SumMBF+$MBF))
    echo $SumMBF > AvgMBF
    CurrMBF="$(printf '%02d:%02d:%02d\n' $(($MBF/3600)) $(($MBF%3600/60)) $(($MBF%60)))"
    echo "---------------------------- ${Gre}Channel Zapping and banner navigation test${RCol} ---------------------------" ;
    echo "                                ""$Bla""$On_Red""¡MIDDLEWARE net.oregan.tvinput.dtv CRASHED!""$RCol""                          ";
    echo "${RCol}${TimeStamp:0:19} -> ${Cya}TEST#$RCol $TestN ${Cya}Ch${RCol} $ChannelN ${Cya}Crashes${RCol} $TotalCrashCount ${Cya}Reboots${RCol} $RebootCount ${Cya}CPU Load${RCol} $LoadCPU ${Cya}Free RAM${RCol} ${FreeRAM}MB${Cya} MBF${RCol} $CurrMBF${RCol}"
    echo "${TimeStamp:0:19},$TestN,$ChannelN,$TotalCrashCount,$RebootCount,$LoadCPU,$FreeRAM,$CurrMBF" >> Testresults;
    start_time="$(date -u +%s)"
    echo $start_time > MBF
    sleep 3
    logcat -c
    sleep 5
    StartDtvPID=$(pidof net.oregan.tvinput.dtv)
    StartMulticastPID=$(pidof net.oregan.tvinput.multicast)
    main
  fi
}

multicast_crash() {
  CurrtMulticastPID=$(pidof net.oregan.tvinput.multicast)
  if [ $StartMulticastPID -eq "$CurrtMulticastPID" ]; then #
    return
  else
    crashLog1=$(logcat -d AndroidRuntime:E DEBUG:F *:S > ./Crashes/$timestamp1-Multicast-Crash.txt)
    MulticastCrashCount=$(($MulticastCrashCount + 1)) #Increase counter.
    TotalCrashCount=$(($TotalCrashCount + 1)) #Increase counter.
    curr_time="$(date -u +%s)"
    MBF="$(($curr_time-$start_time))"
    SumMBF=$(($SumMBF+$MBF))
    echo $SumMBF > AvgMBF
    CurrMBF="$(printf '%02d:%02d:%02d\n' $(($MBF/3600)) $(($MBF%3600/60)) $(($MBF%60)))"
    echo "---------------------------- ${Gre}Channel Zapping and banner navigation test${RCol} ---------------------------" ;
    echo "                                ""$Bla""$On_Red""¡MIDDLEWARE net.oregan.tvinput.multicast CRASHED!""$RCol""                         ";
    echo "${RCol}${TimeStamp:0:19} -> ${Cya}TEST#$RCol $TestN ${Cya}Ch${RCol} $ChannelN ${Cya}Crashes${RCol} $TotalCrashCount ${Cya}Reboots${RCol} $RebootCount ${Cya}CPU Load${RCol} $LoadCPU ${Cya}Free RAM${RCol} ${FreeRAM}MB${Cya} MBF${RCol} $CurrMBF${RCol}"
    echo "${TimeStamp:0:19},$TestN,$ChannelN,$TotalCrashCount,$RebootCount,$LoadCPU,$FreeRAM,$CurrMBF" >> Testresults;
    start_time="$(date -u +%s)"
    echo $start_time > MBF
    sleep 3
    logcat -c
    sleep 5
    StartMulticastPID=$(pidof net.oregan.tvinput.multicast)
    StartDtvPID=$(pidof net.oregan.tvinput.dtv)
    main
  fi
}

#Checks whether live tv end crashed.
livetv_crash() {
  CurrtLivetvPID=$(pidof com.dotscreen.megacable.livetv)
  if [ "$StartLivetvPID" -eq "$CurrtLivetvPID" ]; then 
    return
  else
    crashLog1=$(logcat -d AndroidRuntime:E DEBUG:F *:S > ./Crashes/$timestamp1-LiveTV-Crash.txt)
    LiveTVcrashCount=$(($LiveTVcrashCount + 1)) #Increase counter.
    TotalCrashCount=$(($TotalCrashCount + 1)) #Increase counter.
    curr_time="$(date -u +%s)"
    MBF="$(($curr_time-$start_time))"
    SumMBF=$(($SumMBF+$MBF))
    echo $SumMBF > AvgMBF
    CurrMBF="$(printf '%02d:%02d:%02d\n' $(($MBF/3600)) $(($MBF%3600/60)) $(($MBF%60)))"
    echo "---------------------------- ${Gre}Channel Zapping and banner navigation test${RCol} ---------------------------" ;
    echo "                 ""$Bla""$On_Red""¡LAUNCHER com.dotscreen.megacable.livetv CRASHED!""$RCol""                          ";
    echo "${RCol}${TimeStamp:0:19} -> ${Cya}TEST#$RCol $TestN ${Cya}Ch${RCol} $ChannelN ${Cya}Crashes${RCol} $TotalCrashCount ${Cya}Reboots${RCol} $RebootCount ${Cya}CPU Load${RCol} $LoadCPU ${Cya}Free RAM${RCol} ${FreeRAM}MB${Cya} MBF${RCol} $CurrMBF${RCol}"
    echo "${TimeStamp:0:19},$TestN,$ChannelN,$TotalCrashCount,$RebootCount,$LoadCPU,$FreeRAM,$CurrMBF" >> Testresults;
    start_time="$(date -u +%s)"
    echo $start_time > MBF
    sleep 5
    logcat -c
    sleep 15
    echo "$RCol""Re-starting Live TV"
    run_profiles
    am start -n com.dotscreen.megacable.livetv/com.dotscreen.tv.MainActivity
    sleep 1
    StartLivetvPID=$(pidof com.dotscreen.megacable.livetv)
    main
  fi
}

#Checks whether portal crashed
portal_crash() {
    CurrtPortalPID=$(pidof com.dotscreen.megacable.portal)
  if [ "$StartPortalPID" -eq "$CurrtPortalPID" ]; then 
    return
  else
    crashLog1=$(logcat -d AndroidRuntime:E DEBUG:F *:S > ./Crashes/$timestamp1-Portal-Crash.txt)
    PortalCrashCount=$(($PortalCrashCount + 1)) #Increase counter.
    TotalCrashCount=$(($TotalCrashCount + 1)) #Increase counter.
    curr_time="$(date -u +%s)"
    MBF="$(($curr_time-$start_time))"
    SumMBF=$(($SumMBF+$MBF))
    echo $SumMBF > AvgMBF
    CurrMBF="$(printf '%02d:%02d:%02d\n' $(($MBF/3600)) $(($MBF%3600/60)) $(($MBF%60)))"
    echo "---------------------------- ${Gre}Channel Zapping and banner navigation test${RCol} ---------------------------" ;
    echo "                 ""$Bla""$On_Red""¡LAUNCHER com.dotscreen.megacable.portal CRASHED!""$RCol""                          ";
    echo "${RCol}${TimeStamp:0:19} -> ${Cya}TEST#$RCol $TestN ${Cya}Ch${RCol} $ChannelN ${Cya}Crashes${RCol} $TotalCrashCount ${Cya}Reboots${RCol} $RebootCount ${Cya}CPU Load${RCol} $LoadCPU ${Cya}Free RAM${RCol} ${FreeRAM}MB${Cya} MBF${RCol} $CurrMBF${RCol}"
    echo "${TimeStamp:0:19},$TestN,$ChannelN,$TotalCrashCount,$RebootCount,$LoadCPU,$FreeRAM,$CurrMBF" >> Testresults;
    start_time="$(date -u +%s)"
    echo $start_time > MBF
    sleep 5
    logcat -c
    sleep 15
    StartPortalPID=$(pidof com.dotscreen.megacable.portal)
    StartLivetvPID=$(pidof com.dotscreen.megacable.livetv)
    input keyevent keyevent KEYCODE_HOME
    input keyevent keyevent KEYCODE_HOME
    run_profiles
    main
  fi
}

#Unsubcrived channel, skipes unsubcrived channel by CH+ or Ch-
runUnsubscribe2(){
  SubsCH2=$(logcat -d | grep errorCode) 
  if [[ $SubsCH2 == *"261"* ]]; then
    logcat -c
    SubsCH2=0
    if [ "$varCount" -ge 100 ]; then
      input keyevent KEYCODE_CHANNEL_DOWN
    else
      input keyevent KEYCODE_CHANNEL_UP
    fi
    TimeStamp=$(date "+%m-%d %H:%M:%S") #Update time stamp.
    timestamp1=$(date "+%Y%m%d%H%M%S") #Update timestamp for logs name.
    SubsCH2=$(logcat -d | grep errorCode) 
    sleep 2
  fi
return
}

runUnsubscribe(){
  SubsCH=$(logcat -d | grep duplicate) 
  while [[ $SubsCH == *"livetv/com.dotscreen.tv.MainActivity#1"* ]]; do
    logcat -c
    SubsCH=0
    input keyevent KEYCODE_BACK
    if [ "$varCount" -ge 100 ]; then
      input keyevent KEYCODE_CHANNEL_DOWN
    else
      input keyevent KEYCODE_CHANNEL_UP
    fi
    TimeStamp=$(date "+%m-%d %H:%M:%S") #Update time stamp.
    timestamp1=$(date "+%Y%m%d%H%M%S") #Update timestamp for logs name.
    sleep 2
    SubsCH=$(logcat -d | grep duplicate) 
  done
return
}

#Random navigation to the right
movRight() {
  for i in $(seq 2); do
    input keyevent KEYCODE_DPAD_RIGHT
    sleep .5
  done
  return
}

#Random navigation to the left
movLeft() {
  for i in $(seq 3); do
    input keyevent KEYCODE_DPAD_LEFT
    sleep .5
  done
  return
}

#Read CPU and RAM values.
load_check() {
  LoadCPU=$(dumpsys cpuinfo | grep TOTAL | cut -d " " -f1)
  SumCPU=$(cat CPU)
  SumCPU=$(($SumCPU + ${LoadCPU:0:2}))
  AvgCPU=$(($SumCPU / $testCount))
  echo $SumCPU > CPU
  FreeRAM=$(cat /proc/meminfo | grep MemFree | cut -d”:” -f2)
  FreeRAM=$((${FreeRAM:5:12} / 1000))
  SumRAM=$(cat RAM)
  SumRAM=$(($SumRAM + $FreeRAM))
  AvgRAM=$(($SumRAM / $testCount))
  echo $SumRAM > RAM 
 return          
}

#Get meantime between failures
mbf_check() {
  curr_time="$(date -u +%s)"
  MBF="$(($curr_time-$start_time))"
  if [ "$TotalCrashCount" -eq 0 ]; then
    HrsMBF="$(printf '%02d.%02d\n' $(($MBF/3600)) $(($MBF%3600/60)))"
  else
    SumMBF=$(cat AvgMBF)
    AvgMBF=$((($MBF+$SumMBF)/$TotalCrashCount))
    HrsMBF="$(printf '%02d.%02d\n' $(($AvgMBF/3600)) $(($AvgMBF%3600/60)))"
  fi
    CurrMBF="$(printf '%02d:%02d:%02d\n' $(($MBF/3600)) $(($MBF%3600/60)) $(($MBF%60)))"
  return
}

#Display interactions and test results
testResult() {
  echo "${TimeStamp:0:19},$TestN,$ChannelN,$TotalCrashCount,$RebootCount,$LoadCPU,${FreeRAM},$CurrMBF" >> TestResults;
  echo "--------------------------- ${Gre}Channel Zapping and banner navigation test${RCol} ----------------------------"
  echo "${RCol}${TimeStamp:0:19} -> ${Cya}TEST#$RCol $TestN ${Cya}Ch${RCol} $ChannelN ${Cya}Crashes${RCol} $TotalCrashCount ${Cya}Reboots${RCol} $RebootCount ${Cya}CPU Load${RCol} ${LoadCPU} ${Cya}Free RAM${RCol} ${FreeRAM}MB${Cya} MBF${RCol} $CurrMBF${RCol}"
  return
} 

start_time=$(cat MBF)
main() {
  Live=$(dumpsys window windows | grep -E 'mFocusedApp') #current app
  while [[ $Live == *"com.dotscreen.megacable.livetv"* ]]; do
    varCount=$(($varCount + 1)) #Increase counter
    testCount=$(($testCount + 1)) #Increase test counter
    TestN=$(printf "%04d" "$testCount") #Counts number of test execute
    runUnsubscribe
    runUnsubscribe2 #Verify if channel requires subscription.
    #RebootCount=$(settings get global boot_count)
    logcat -c

    if [ "$varCount" -ge 100 ]; then #Verifies counter of variable varCount to cycle 100 times Ch down and 100 times Ch up
      for i in $(seq 3); do #Runs channel down for n times
        input keyevent KEYCODE_CHANNEL_DOWN #Channel down
        TimeStamp=$(date "+%m-%d %H:%M:%S") #Update time stamp
        timestamp1=$(date "+%Y-%m-%d_%H-%M-%S") #Update timestamp for logs name
        sleep .5
      done
    else
      for i in $(seq 2); do #Runs channel up for n times
        input keyevent KEYCODE_CHANNEL_UP
        TimeStamp=$(date "+%m-%d %H:%M:%S") #Update time stamp
        timestamp1=$(date "+%Y-%m-%d_%H-%M-%S") #Update timestamp for logs name
        sleep .5 
      done
    fi
      sleep 5
      runUnsubscribe #Verify if channel requires subscription.
      runUnsubscribe2 #Verify if channel requires subscription.
      sleep 5
      Channel=$(logcat -d | grep displayNumber | grep onZap | cut -d "," -f5 | cut -b16-19) #Update current channel
      ChannelN=$(printf "%04d" "${Channel:(-4)}")
      movLeft #Moves to the left in catchup content
      movRight #Moves to the right in catchup content
      sleep 1
      sys_crash #Verifies if system server crashed
      portal_crash #Verifies if portal crashed
      livetv_crash #Verifies if live tv crashed
      dtv_crash #Verify again if dtv crashed
      multicast_crash #Verify again if multicast crashed
      mbf_check
      load_check
      testResult #Prints test results and interactions 
      run_profiles #If launcher crashes and need to input profile PIN 

    #Every 10 tests, it will perform a manual channel tune of 100, 161 and 204 channels
    if [ "${TestN:(-1)}" == "9" ]; then
      testCount=$(($testCount + 1)) #Increase test counter
      TestN=$(printf "%04d" "$testCount") #Counts number of test executed
      ManualCh="100 161 204 1000 1161 1204 1212"
      for i in $ManualCh; do
        input text $i
        TimeStamp=$(date "+%m-%d %H:%M:%S") #Update time stamp
        timestamp1=$(date "+%Y-%m-%d_%H-%M-%S") #Update timestamp for logs name
        sleep 10
      done
      #RebootCount=$(settings get global boot_count)
      runUnsubscribe #Verify if channel requires subscription.
      runUnsubscribe2 #Verify if channel requires subscription.
      sys_crash #Verifies if system server crashed
      portal_crash #Verifies if portal crashed
      livetv_crash #Verifies if live tv crashed
      dtv_crash #Verify again if dtv crashed
      multicast_crash #Verify again if multicast crashed
      mbf_check 
      load_check
      echo "${TimeStamp:0:19},$TestN,100 161 204 1000 1161 1204 1212,$TotalCrashCount,$RebootCount,$LoadCPU,${FreeRAM},$CurrMBF" >> TestResults;
      echo "------------- ${BYel}Select channels 100, 161, 204, 1000, 1161, 1204 & 1212 using numeric keys${RCol} -----------";
      echo "${RCol}${TimeStamp:0:19} -> ${Cya}TEST#$RCol $TestN ${Cya}Ch${RCol} $ChannelN ${Cya}Crashes${RCol} $TotalCrashCount ${Cya}Reboots${RCol} $RebootCount ${Cya}CPU Load${RCol} $LoadCPU ${Cya}Free RAM${RCol} ${FreeRAM}MB${Cya} MBF${RCol} $CurrMBF${RCol}"
      input text "${ChannelN}"; #Backs to previus saved channel by the script
      sleep 5
    fi

    #Channel change counter
    if [ "$varCount" -ge 200 ]; then
      varCount=0
    fi

    #If Channel reaches the bottom, changes to CH+ cycle
    if [[ $ChannelN -lt 102 ]]; then
      input text "100"
      sleep .5 
      input keyevent KEYCODE_DPAD_CENTER
      varCount=0
    fi

    #If Channel reaches the top, changes to CH- cycle
    if [[ $ChannelN -eq 1702 ]]; then
      varCount=101
    fi
    
    #Finishes script at 100 tests and pull reportsn
    echo "Instability Events ${timestamp1}" > InstabilityEvents
    echo "Tests Executed,LiveTV Crashes,Portal Crashes,Dtv Crashes,Multicast Crashes,Sys_server Crashes,Total Crashes,Reboots,Avg CPU Load,Avg Free RAM (MB),Avg MBF (Hrs)" >> InstabilityEvents
    echo "$TestN,$LiveTVcrashCount,$PortalCrashCount,$DtvCrashCount,$MulticastCrashCount,$SysCrashCount,$TotalCrashCount,$RebootCount,$AvgCPU,$AvgRAM,$HrsMBF" >> InstabilityEvents
    
    logcat -c
    sleep 10
    Live2=$(dumpsys window windows | grep -E 'mCurrentFocus') #current app
    if [[ $Live2 != *"SubPanel:com.dotscreen.megacable.livetv/com.dotscreen.tv.MainActivity"* ]]; then
      input keyevent KEYCODE_BACK
    fi
  done
}
  Live=$(dumpsys window windows | grep -E 'mFocusedApp') #current app
  if [[ $Live != *"com.dotscreen.megacable.livetv/com.dotscreen.tv.MainActivity"* ]]; then
    am start -n com.dotscreen.megacable.livetv/com.dotscreen.tv.MainActivity
  fi
main
exit