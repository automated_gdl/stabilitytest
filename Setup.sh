#!/bin/bash
#This test script aims to stress middleware and launcher.

Bla='\e[0;30m'; On_Red='\e[41m'; On_Gre='\e[42m'; BYel='\e[1;33m'; Cya='\e[0;36m'; BIBlu='\e[1;94m'; Gre='\e[0;32m'; Red='\e[0;31m'; Pur='\e[0;35m'; # Text colors
RCol='\e[0m' # Text Reset

sleep 15

settings put secure sleep_timeout -1 #Set sleep tomeput to never

#Completes profiles screen by selecting main profile and input default PIN (0000).
run_profiles() {
Profiles=$(dumpsys window windows | grep -E mCurrentFocus)
 if [[ $Profiles == *"ProfileManagementActivity"* ]]; then
    sleep 5
    input keyevent KEYCODE_BACK      #Closes PIN menu if opened
    input keyevent KEYCODE_DPAD_DOWN #LOCATES SELECTION TO MAIN PROFILE
    input keyevent KEYCODE_DPAD_DOWN 
    input keyevent KEYCODE_DPAD_LEFT
    input keyevent KEYCODE_DPAD_LEFT
    input keyevent KEYCODE_DPAD_LEFT
    input keyevent KEYCODE_DPAD_LEFT 
    input keyevent KEYCODE_DPAD_CENTER
    sleep 1
    input text "0000"
  fi
  return
}

sleep 5
run_profiles

#scan process:
scan(){
logcat -c
echo ${BIBlu}"Performing a channel scan..."
sleep 2
input keyevent KEYCODE_HOME
sleep 3
type=$(logcat -d | grep "HomeHeroZoneView: tifChannel")
for i in $(seq 7); do
  input keyevent KEYCODE_DPAD_DOWN
done
for i in $(seq 4); do
  input keyevent KEYCODE_DPAD_LEFT
done
input keyevent KEYCODE_DPAD_CENTER
if [[ $type == *"TYPE_DVB_C"* ]]; then
  for i in $(seq 4); do
    input keyevent KEYCODE_DPAD_RIGHT
    Apro="DVB"
  done
else
  for i in $(seq 3); do
    input keyevent KEYCODE_DPAD_RIGHT
    Apro="IPTV"
  done
fi
input keyevent KEYCODE_DPAD_DOWN
input keyevent KEYCODE_DPAD_CENTER

SResult=0
sleep 20
}

scan 

#For scan success
Scan=$(logcat -d | grep SCAN_STATUS_TIF_UPDATE_DONE)
if [[ $Scan == *"SCAN_STATUS_OK"* ]]; then
  SResult=$Gre"Complete!"$RCol
  echo "Scan process" "$SResult", "$BIBlu""Channels found:""${RCol}""$Gre""${Scan:(-3)}""$RCol"
fi

Scan1=$(logcat -d | grep SCAN_STATUS_ERROR)
#For scan fail
if [[ $Scan1 == *"No channel found"* ]]; then
  echo "${BIBlu}""Scan process" ${On_Red}"FAIL"${RCol}
  sleep 3
  scan
fi

Scan2=$(logcat -d | grep SCAN_STATUS_ERROR)
#For scan fail
if [[ $Scan2 == *"null"* ]]; then
  echo "${BIBlu}""Scan process"${On_Red}"FAIL"${RCol}
  sleep 3
  scan
fi

cd sdcard/Automation
echo $(getprop ro.boot.bootreason) >> Boot_Reasons
#Reads device under test information
Model=$(getprop ro.product.vendor.device)
Snum=$(getprop ro.serialno)
MacA=$(getprop ro.boot.mac)
Aver=$(getprop ro.build.version.release)
Build=$(getprop ro.product.version.base)
Launcher=$(dumpsys package com.dotscreen.megacable.livetv | grep versionName | cut -d "=" -f2)
IPv4=$(ip route | cut -d " " -f9)
#Apro=$(logcat -d | grep modelExternalId | cut -d " " -f 11)
timestamp1=$(date "+%Y-%m-%d_%H-%M-%S") #Update timestamp for logs name
startDate=$(date "+%Y-%m-%d")
crashLog1=0; LiveTVcrashCount=0; DtvCrashCount=0; SysCrashCount=0; DtvServCrashCount=0; MultiServCrashCount=0; MulticastCrashCount=0; PortalCrashCount=0; TotalCrashCount=$(sed -n 3p InstabilityEvents | cut -d "," -f11); varCount=0; testCount=$(sed -n 3p InstabilityEvents | cut -d "," -f1); CountTIF=0
TestN=0; vCount=0; aCount=0; ChannelN=0; LoadCPU=0; FreeRAM=0; AvgCPU=0; SumCPU=0; AvgRAM=0; SumRAM=0; MBF=0; AvgMBF=0; SumMBF=0; TIF=0; RebootCount=0
TestCount=$(printf "%03d" "$TestCount")
TotalCrashCount=$(printf "%03d" "$TotalCrashCount")
sleep .5

#Prints device under test information.
echo
echo "$BYel""----------DEVICE INFO----------""$RCol"
echo "$BIBlu""Model:""$RCol" "$Model""-""$Apro"
echo "$BIBlu""Serial Number:""$RCol" "$Snum"
echo "$Model"-"$Apro" > DeviceInfo
echo "Serial Number","$Snum" >> DeviceInfo
echo "$BIBlu""MAC Address:""$RCol" "$MacA"
echo "MAC Address","$MacA" >> DeviceInfo
echo "$BIBlu""IP Address:""$RCol" "$IPv4"
echo "$BIBlu""Android:""$RCol" "$Aver"
echo "$BIBlu""Build:""$RCol" "${Build:0:6}"
echo "Build","${Build:0:6}" >> DeviceInfo
echo "$BIBlu""Launcher:""$RCol" "${Launcher:0:6}"
echo "Launcher","${Launcher:0:6}" >> DeviceInfo
echo

echo "${BIBlu}""Test set up in progress...""${RCol}"
sleep 10
run_profiles
sleep 10

#Here, script will start with the automatic set up, go to home and tune 100 channel.
input keyevent KEYCODE_HOME
sleep 2
input keyevent KEYCODE_HOME
input text "1212"
sleep .5
input keyevent KEYCODE_DPAD_CENTER
sleep 5
Channel=$(logcat -d | grep displayNumber | grep onZap | cut -d "," -f5 | cut -b16-19) #Update current channel.
ChannelN=$(printf "%04d" "${Channel:(-4)}")

exit