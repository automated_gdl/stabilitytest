@ECHO OFF
::
PATH=%PATH%

set start_date=%DATE%
set /a count=0

goto main

::------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:reset
echo "Generating daily report..."
taskkill /fi "WindowTitle eq Stability Test STB-*"
start /HIGH RunReport.au3
timeout /t 5 /nobreak > nul

FOR /F %%x IN (Devices.txt) DO (
    adb -s %%x pull /sdcard/Automation/TestResults "C:\Users\QA-Automation\Google Drive\QA-Automation-Android-GDL\ATR\StabilityTest\%start_date%\STB-%%x\TestResults.csv"
    adb -s %%x pull /sdcard/Automation/InstabilityEvents "C:\Users\QA-Automation\Google Drive\QA-Automation-Android-GDL\ATR\StabilityTest\%start_date%\STB-%%x\InstabilityEvents.csv"
    adb -s %%x pull /sdcard/Automation/DeviceInfo "C:\Users\QA-Automation\Google Drive\QA-Automation-Android-GDL\ATR\StabilityTest\%start_date%\STB-%%x\DeviceInfo.csv"
    adb -s %%x pull /sdcard/Automation/Crashes "C:\Users\QA-Automation\Google Drive\QA-Automation-Android-GDL\ATR\StabilityTest\%start_date%\STB-%%x"
    adb -s %%x pull /sdcard/Automation/Boot_Reasons "C:\Users\QA-Automation\Google Drive\QA-Automation-Android-GDL\ATR\StabilityTest\%start_date%\STB-%%x\Boot_Reasons.csv"
    mkdir "C:\Users\QA-Automation\Google Drive\QA-Automation-Android-GDL\ATR\StabilityTest\StabilityResults\%DATE%\STB-%%x"
)

echo "Setting up a new test run..."

FOR /F %%x IN (Devices.txt) DO ( 
    adb -s %%x shell mkdir sdcard/Automation/
    adb -s %%x shell "echo $(date -u +%%s) > /sdcard/Automation/MBF"
)
xcopy "C:\Users\QA-Automation\Google Drive\QA-Automation-Android-GDL\ATR\StabilityTest\StabilityResults\%start_date%\Android-Daily Stability Report.xlsm"  "C:\Users\QA-Automation\Google Drive\QA-Automation-Android-GDL\ATR\StabilityTest\StabilityResults\%DATE%\"
echo Test Results %DATE% > TestResults | echo Instability Events %DATE% > InstabilityEvents | echo Boot Reasons %DATE% > Boot_Reasons
echo Timestamp,TEST #,Channel,Crashes,Reboots,CPU Load,Free RAM (MB),MBF >> TestResults
echo Tests Executed,LiveTV Crashes,Portal Crashes,Dtv Crashes,Multicast Crashes,Sys_server Crashes,Total Crashes,Reboots,Avg CPU Load,Avg Free RAM (MB),Avg MBF (Hrs) >> InstabilityEvents
echo 0,0,0,0,0,0,0,0,0,0,0 >> InstabilityEvents
set start_date=%DATE%
set /a count=0

FOR /F %%x IN (Devices.txt) DO (
    adb -s %%x shell rm -rf /sdcard/Automation/Crashes/
    adb -s %%x shell rm -rf /sdcard/Automation/CPU
    adb -s %%x shell rm -rf /sdcard/Automation/RAM
    adb -s %%x shell rm -rf /sdcard/Automation/Boot_Reasons
    adb -s %%x shell mkdir sdcard/Automation/Crashes/
    adb -s %%x push StabilityTestScript.sh Setup.sh TestResults Boot_Reasons InstabilityEvents /sdcard/Automation/
    adb -s %%x shell settings put global boot_count 0
    start "Setup STB-%%x" /HIGH cmd /c "adb -s %%x shell sh /sdcard/Automation/Setup.sh"
    timeout /t 2 /nobreak > nul
)
timeout /t 90 /nobreak > nul
::------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
echo "Launching Stability script in all the STBs..."

:main
::Re-starts script if killed
FOR /F %%x IN (Devices.txt) DO (
    tasklist /FI "WINDOWTITLE eq Stability Test STB-%%x" | findstr /B "INFO:" > nul && start "Stability Test STB-%%x" /HIGH cmd /c "adb -s %%x shell sh /sdcard/Automation/StabilityTestScript.sh"
)

if %start_date% NEQ %DATE% goto reset 

timeout /t 10 /nobreak > nul
if %count%==1080 goto pull
set /a count+=1
goto main

::-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
::Pull output files from STBs
:pull
echo Pulling output files from all STBs...
FOR /F %%x IN (Devices.txt) DO (
    adb -s %%x pull /sdcard/Automation/TestResults "C:\Users\QA-Automation\Google Drive\QA-Automation-Android-GDL\ATR\StabilityTest\StabilityResults\%DATE%\STB-%%x\TestResults.csv"
    adb -s %%x pull /sdcard/Automation/InstabilityEvents "C:\Users\QA-Automation\Google Drive\QA-Automation-Android-GDL\ATR\StabilityTest\StabilityResults\%DATE%\STB-%%x\InstabilityEvents.csv"
    adb -s %%x pull /sdcard/Automation/DeviceInfo "C:\Users\QA-Automation\Google Drive\QA-Automation-Android-GDL\ATR\StabilityTest\StabilityResults\%DATE%\STB-%%x\DeviceInfo.csv"
    adb -s %%x pull /sdcard/Automation/Crashes "C:\Users\QA-Automation\Google Drive\QA-Automation-Android-GDL\ATR\StabilityTest\StabilityResults\%DATE%\STB-%%x"
    adb -s %%x pull /sdcard/Automation/Boot_Reasons "C:\Users\QA-Automation\Google Drive\QA-Automation-Android-GDL\ATR\StabilityTest\StabilityResults\%DATE%\STB-%%x\Boot_Reasons.csv"
)
set /a count=0
goto main

echo Press any key to exit...
pause > nul
exit