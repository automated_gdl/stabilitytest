#include <Excel.au3>
#include <MsgBoxConstants.au3>
#include <Date.au3>
#include <File.au3>

Local $yesterdayx = _DateAdd('d', -1, _NowCalcDate())
Local $yesterday = _DateTimeFormat ( $yesterdayx, 2 )
Local $oExcel = _Excel_Open()
Local $mfile = @ScriptDir & "\StabilityResults\" & $yesterday & "\Android-Daily Stability Report.xlsm"
Local $oDailyReport = _Excel_BookOpen($oExcel, $mfile)
WinWait ("[CLASS:#32770]")
$id = WinGetHandle("[CLASS:#32770]")
ControlSend($id, "", "","{ENTER}")
Sleep(5000)

$file = "./Devices.txt"
FileOpen($file, 0)
For $i = 1 to _FileCountLines($file)
    $line = FileReadLine($file, $i)
    Local $mfile2 = @ScriptDir & "\StabilityResults\" & $yesterday & "\STB-" & $line & "\InstabilityEvents.csv"
    Local $oInstabilityEvents = _Excel_BookOpen($oExcel, $mfile2)
    Sleep(5000)
    Local $cInstabilityEvents = _Excel_BookClose($oInstabilityEvents)
Next
FileClose($file)
Sleep(5000)
Local $sDailyReport = _Excel_BookSave ($oDailyReport)
Sleep(5000)
Local $sOutput = @ScriptDir & "\StabilityResults\" & $yesterday & "\Android-Daily Stability Report-" & $yesterday & ".pdf"
Local $oConvertPDF = _Excel_Export ($oExcel, $oDailyReport, $sOutput)
Sleep(10000)
$oExcel.Quit()
local $sCopyFrom = @ScriptDir & "\StabilityResults\" & $yesterday & "\"
local $sCopyTo = @ScriptDir & "\StabilityResults\Daily reports\"
local $sFile = "Android-Daily Stability Report-" & $yesterday & ".pdf"
FileCopy($sCopyFrom & $sFile, $sCopyTo & $sFile)
Sleep(5000)